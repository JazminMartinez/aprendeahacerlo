import { Request, Response, NextFunction } from "express";
import { Schedule } from "../entities/Schedule.entity";
import { getRepository } from "typeorm";
import { Workshop } from "../entities/Workshop.entity";

export const createSchedule = async(req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    const {day, startTime,endTime } = req.body;

    try {
        const workshop = await getRepository(Workshop).findOne(id);
        const newSchedule = getRepository(Schedule).create({
          day,startTime,endTime, workshop
        });
        
        getRepository(Schedule).save(newSchedule)
        .then((schedule) => {
            res.status(200).json({ message: "Schedule registrado con exito", schedule});
          })
          .catch((err) => {
            console.log(err);
            res.status(500).json({ message: "Error", error: err });
          });
      } catch (err) {
        res.status(500).json({message:"ocurrio un error" ,error: err });
      }
}

export const validateDay=async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const {day}=req.body;
  let dayAvailable:boolean;
  try {
    const schedule = await getRepository(Schedule)
      .createQueryBuilder("schedule")
      .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
      .andWhere("schedule.day = :day", { day: day })
      .execute();
      console.log(schedule);
      if(schedule){
        res.status(200).json({dayAvailable:false});
      }else{
        res.status(200).json({dayAvailable:true});
      }
  } catch (err) {
    res.status(500).json({ message: "Error", error: err });
  }
};

export const getSchedules = async(req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    try {
        const schedules = await getRepository(Schedule)
          .createQueryBuilder("schedule")
          .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
          .execute();
          res.status(200).json({schedules});
      } catch (err) {
        res.status(500).json({ message: "Error", error: err });
      }
}

export const editSchedule = async(req: Request, res: Response, next: NextFunction) => {
    const { id,idS } = req.params;
    const {day, startTime,endTime } = req.body;
    try {
        await getRepository(Schedule)
        .createQueryBuilder()
        .update(Schedule)
        .set({ 
        day,startTime,endTime
    })
    .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
    .andWhere("schedule.id = :id", {id: idS })
    .execute();
    const scheduleActualizado = await getRepository(Schedule).findOne(idS);
    res.status(200).json({message:"schedule actualizado",scheduleActualizado});
      } catch (err) {
        res.status(500).json({message:"ocurrio un error" ,error: err });
      }
}

export const deleteSchedule = async(req: Request, res: Response, next: NextFunction) => {
    const { id,idS } = req.params;
    try {
    await getRepository(Schedule)
    .createQueryBuilder()
    .delete()
    .from(Schedule)
    .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
    .andWhere("schedule.id = :id", {id: idS })
    .execute();

    res.status(200).json({message:"schedule eliminado"});
      } catch (err) {
        res.status(500).json({message:"ocurrio un error" ,error: err });
      }
}

