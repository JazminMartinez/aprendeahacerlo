import { Entity, PrimaryGeneratedColumn,ManyToOne,JoinColumn,CreateDateColumn, Column, Timestamp } from "typeorm";
import { Workshop } from './Workshop.entity';
import { time } from "console";

@Entity()
export class Schedule{
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    day:string;

    @CreateDateColumn ({type:"time"})
    startTime:Timestamp;

    @CreateDateColumn ({type:"time"})
    endTime:Timestamp;

    @ManyToOne(type => Workshop, workshop => workshop.schedule,{ onDelete: 'CASCADE' })
    @JoinColumn({ name: 'workshop_id' }) 
    workshop: Workshop;
}