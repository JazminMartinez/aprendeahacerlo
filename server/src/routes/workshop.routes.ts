import { Router } from 'express';
import{createWorkshop,getWorkshop,getAllWorkshops,getWorkshopsUser,subscribe,
    mostRecentWorkshops,editWorkshop,unsubscribe,deleteWorkshop, workshopsByCategories,
    validateWorkshop
} from '../controllers/worskshop.controller';

const router = Router();

router.post('/createWorkshop', createWorkshop);
router.post('/validateWorkshop', validateWorkshop);
router.get('/workshop/:id', getWorkshop);
router.put('/editWorkshop/:id', editWorkshop);
router.delete('/deleteWorkshop/:id', deleteWorkshop);

router.get('/subscribe/:id', subscribe);
router.delete('/unsubscribe/:id', unsubscribe);
router.get('/getAllWorkshops', getAllWorkshops);
router.get('/workshops/:name', workshopsByCategories);
router.get('/workshopsUser', getWorkshopsUser);
router.get('/mostRecentWorkshops', mostRecentWorkshops);

export default router;