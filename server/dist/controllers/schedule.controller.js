"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteSchedule = exports.editSchedule = exports.getSchedules = exports.validateDay = exports.createSchedule = void 0;
var Schedule_entity_1 = require("../entities/Schedule.entity");
var typeorm_1 = require("typeorm");
var Workshop_entity_1 = require("../entities/Workshop.entity");
exports.createSchedule = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, day, startTime, endTime, workshop, newSchedule, err_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, day = _a.day, startTime = _a.startTime, endTime = _a.endTime;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 2:
                workshop = _b.sent();
                newSchedule = typeorm_1.getRepository(Schedule_entity_1.Schedule).create({
                    day: day, startTime: startTime, endTime: endTime, workshop: workshop
                });
                typeorm_1.getRepository(Schedule_entity_1.Schedule).save(newSchedule)
                    .then(function (schedule) {
                    res.status(200).json({ message: "Schedule registrado con exito", schedule: schedule });
                })
                    .catch(function (err) {
                    console.log(err);
                    res.status(500).json({ message: "Error", error: err });
                });
                return [3 /*break*/, 4];
            case 3:
                err_1 = _b.sent();
                res.status(500).json({ message: "ocurrio un error", error: err_1 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.validateDay = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, day, dayAvailable, schedule, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                day = req.body.day;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Schedule_entity_1.Schedule)
                        .createQueryBuilder("schedule")
                        .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
                        .andWhere("schedule.day = :day", { day: day })
                        .execute()];
            case 2:
                schedule = _a.sent();
                console.log(schedule);
                if (schedule) {
                    res.status(200).json({ dayAvailable: false });
                }
                else {
                    res.status(200).json({ dayAvailable: true });
                }
                return [3 /*break*/, 4];
            case 3:
                err_2 = _a.sent();
                res.status(500).json({ message: "Error", error: err_2 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.getSchedules = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, schedules, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Schedule_entity_1.Schedule)
                        .createQueryBuilder("schedule")
                        .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
                        .execute()];
            case 2:
                schedules = _a.sent();
                res.status(200).json({ schedules: schedules });
                return [3 /*break*/, 4];
            case 3:
                err_3 = _a.sent();
                res.status(500).json({ message: "Error", error: err_3 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.editSchedule = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, id, idS, _b, day, startTime, endTime, scheduleActualizado, err_4;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                _a = req.params, id = _a.id, idS = _a.idS;
                _b = req.body, day = _b.day, startTime = _b.startTime, endTime = _b.endTime;
                _c.label = 1;
            case 1:
                _c.trys.push([1, 4, , 5]);
                return [4 /*yield*/, typeorm_1.getRepository(Schedule_entity_1.Schedule)
                        .createQueryBuilder()
                        .update(Schedule_entity_1.Schedule)
                        .set({
                        day: day, startTime: startTime, endTime: endTime
                    })
                        .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
                        .andWhere("schedule.id = :id", { id: idS })
                        .execute()];
            case 2:
                _c.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Schedule_entity_1.Schedule).findOne(idS)];
            case 3:
                scheduleActualizado = _c.sent();
                res.status(200).json({ message: "schedule actualizado", scheduleActualizado: scheduleActualizado });
                return [3 /*break*/, 5];
            case 4:
                err_4 = _c.sent();
                res.status(500).json({ message: "ocurrio un error", error: err_4 });
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
exports.deleteSchedule = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, id, idS, err_5;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.params, id = _a.id, idS = _a.idS;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Schedule_entity_1.Schedule)
                        .createQueryBuilder()
                        .delete()
                        .from(Schedule_entity_1.Schedule)
                        .where("schedule.workshop_id = :workshop_id", { workshop_id: id })
                        .andWhere("schedule.id = :id", { id: idS })
                        .execute()];
            case 2:
                _b.sent();
                res.status(200).json({ message: "schedule eliminado" });
                return [3 /*break*/, 4];
            case 3:
                err_5 = _b.sent();
                res.status(500).json({ message: "ocurrio un error", error: err_5 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
