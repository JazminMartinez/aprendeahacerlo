import { Injectable } from '@angular/core';
import { AsyncValidator, FormControl } from '@angular/forms';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthService } from 'src/app/services/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class UniqueAccountService implements AsyncValidator {

  constructor(private auth: AuthService) { }
  validate = (control: FormControl) => {
    const { value } = control;
    return this.auth.validateAccount(value).pipe(
        map(resp => null),
        catchError(err => {
          if (!err.error.available) {
            return of({ notAvailableAccount: true});
          } else {
            return of({ noConnection: true});
          }
        })
      );
  }
}
