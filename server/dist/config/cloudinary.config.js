"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cloudinary_1 = require("cloudinary");
var CloudinaryStorage = require('multer-storage-cloudinary').CloudinaryStorage;
var multer = require('multer');
cloudinary_1.v2.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_KEY,
    api_secret: process.env.CLOUDINARY_SECRET
});
var storage = new CloudinaryStorage({
    cloudinary: cloudinary_1.v2,
    params: {
        folder: "imagenes",
        allowed_formats: ['mp4', 'ogv', 'jpg', 'png', 'pdf', 'gif']
    }
});
var uploadCloud = multer({ storage: storage });
exports.default = uploadCloud;
