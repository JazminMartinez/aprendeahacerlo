import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth-service.service';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user.interface';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [
  ]
})
/**
 * Para poder mostrar las diferentes paginas que puede visitar un usuario se llama al servicio y se verifica
 * que este loggeado. y utilizando una variable booleana se hace el cambio para mostrar en el navbar las rutas.
 */
export class NavbarComponent implements OnInit {
  toggle = false;
  isAuthenticated: boolean;
  user:User;
  constructor(private auth: AuthService,private router: Router) { }

  ngOnInit(): void {
    this.auth.isAutheticated.subscribe(credetials => {
      this.isAuthenticated = credetials;
    });
    this.auth.verifyAuth().subscribe(resp => {});
  }

  //llama al servicio de logout y elimina la session y envia al usuario a la pagina principal
  logout(){
    this.auth.logout().subscribe({
      next: () => {
        this.router.navigateByUrl('');
      },
      error: (err) => {
        console.log(err);
      }
    });
  }
}
