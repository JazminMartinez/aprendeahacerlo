import nodemailer from 'nodemailer';
import { Workshop } from '../entities/Workshop.entity';
import { User } from '../entities/User.entity';

const transporter = nodemailer.createTransport({      
    host: 'smtp.gmail.com',
    auth: {
      user: "jazmin.94mh@gmail.com",
      pass: "sarangha3."
    }
  });

  export const confirmAccount= async (para:string, workshop:Workshop,user:any)=>{
    const  day1 = ("0" + workshop.startDate.getDate()).slice(-2);
    const month1 = ("0" + (workshop.startDate.getMonth() + 1)).slice(-2);
    const fecha1 = workshop.startDate.getFullYear()+"-"+(month1)+"-"+(day1) ;

    const  day2 = ("0" + workshop.endDate.getDate()).slice(-2);
    const month2= ("0" + (workshop.endDate.getMonth() + 1)).slice(-2);
    const fecha2 = workshop.endDate.getFullYear()+"-"+(month2)+"-"+(day2) ;

    return await transporter.sendMail({
        from: '"Aprendiendo. Registro de talleres en linea. 😎" <jazmin.94mh@gmail.com>', // sender address
        to: para, // list of receivers
        subject: "Correo de confirmación de inscripción a taller", // Subject line
        html: `
        <p style="text-align: center;"><img src="https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG" alt="Logotipo" width="226" height="108" /></p>
            <h2 style="text-align: center;"><strong>Bienvenid@ ${user["name"]} a ${workshop.name}</strong></h2>
            <div style="text-align: center;"><strong>&nbsp;<h2>Fecha de inicio:</h2> <input type="date" name="fecha"  value="${fecha1}"></strong></div>
            <div style="text-align: center;"><strong><h2>Fecha de termino:</h2> <input type="date" name="fecha"  value="${fecha2}"></strong></div>
            <div style="text-align: center;">&nbsp;</div>
            <h3 style="text-align: center;"><strong>Podr&aacute;s ver m&aacute;s detalles de este taller, ingresando a tu perfil.</strong></h3>`,
    })
  }

  export const confirmEdit= async (usuario:User, workshop:Workshop)=>{

    return await transporter.sendMail({
        from: '"Aprendiendo. Registro de talleres en linea. 😎" <jazmin.94mh@gmail.com>', // sender address
        to: usuario.email, // list of receivers
        subject: "Taller actualizado", // Subject line
        html: `
        <p style="text-align: center;"><img src="https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG" alt="Logotipo" width="226" height="108" /></p>
            <h2 style="text-align: center;"><strong>${workshop.name}</strong></h2>
            <h3 style="text-align: center;"><strong>${usuario["name"]} por este medio, se te informa que el taller ha sido modificado.
            Lamentamos el inconveniente y eperamos verlo en los nuevos horarios</strong></h3>
            <h3 style="text-align: center;"><strong>Podr&aacute;s ver m&aacute;s detalles de este taller, ingresando a tu perfil.</strong></h3>
            <div style="text-align: center;">&nbsp;</div>
            `,
    })
  }

  export const confirmDelete= async (usuario:User, workshop:Workshop)=>{
    return await transporter.sendMail({
        from: '"Aprendiendo. Registro de talleres en linea. 😎" <jazmin.94mh@gmail.com>', // sender address
        to: usuario.email, // list of receivers
        subject: "Taller eliminado", // Subject line
        html: `
        <p style="text-align: center;"><img src="https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG" alt="Logotipo" width="226" height="108" /></p>
            <h2 style="text-align: center;"><strong>${workshop.name}</strong></h2>
            <h3 style="text-align: center;"><strong>${usuario["name"]} por este medio, se te informa que el taller ha sido eliminado 
            y a partir de hoy quedan canceladas las clases.
            Lamentamos las molestias y esperamos que considere inscribirse a otro taller.</strong></h3>
            <div style="text-align: center;">&nbsp;</div>
          `,
    })
  }