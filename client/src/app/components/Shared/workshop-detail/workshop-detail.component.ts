import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { WorkshopServiceService } from 'src/app/services/workshop-service.service';
import { Workshop } from 'src/app/interfaces/workshop.interface';
import { Schedule } from 'src/app/interfaces/schedule.interface';
import { User } from 'src/app/interfaces/user.interface';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-workshop-detail',
  templateUrl: './workshop-detail.component.html',
  styles: [
  ]
})
export class WorkshopDetailComponent implements OnInit {
  workshop:Workshop;
  schedule:Schedule;
  nohay:boolean=false;
  user:User;
  student:boolean=false;
  mensaje="";
  edit=false;
  editForm:FormGroup;
  constructor(private router: ActivatedRoute,private wss:WorkshopServiceService,private router2:Router,private fb:FormBuilder) { }

  ngOnInit(): void {
    //creamos un formulario reactivo con formbuilder, con los campos del taller
    this.editForm=this.fb.group({
      name: ["", [Validators.required, Validators.pattern(/^[a-zA-Z]*$/)]],
      description: ["", [Validators.required, Validators.pattern(/^[a-zA-Z]*$/)]],
      category:["",[Validators.required]],
      createdAt:["",[Validators.required]],
      startDate:["",[Validators.required]],
      endDate:["",[Validators.required]],
      capacity:["",[Validators.required, Validators.pattern(/^[0-9]{1,2}$/)]]
    });

    //asigna la informacion traida del resolver
    this.router.data.subscribe(data => {
      this.workshop = data.workshop;
      this.schedule=data.schedule.schedules;
      console.log(this.schedule)
      //checa si el array tiene 0 elementos y devuelve un mensaje
      if(data.schedule.schedules.length===0){
         this.nohay=true;
         this.mensaje=`No hay horarios por mostrar en ${this.workshop.name}`
       }else{
         this.nohay=false;
       }
       //para ocultar o mostrar elementos verifica el rol del usuario
       this.user=data.user.user;
      if(this.user.role==='student'){
        this.student=true;
      }else{
        this.student=false;
      }
    })
  }

  //llama al servicio para eliminar el taller seleccionado
  eliminarTaller(id:number){
    this.wss.deleteWorkshop(id).subscribe();
    this.router2.navigateByUrl('profile/workshopsUser')
  }

  //llama al servicio para eliminar la relacion entre alumno y taller
  salirTaller(id:number){
    this.wss.unsubscribeWorkshop(id).subscribe();
    this.router2.navigateByUrl('profile/workshopsUser')
  }

  //cuando da click a editar se muestra el formulario para editar el taller
  editarTaller(){
    this.edit=true;
  }

  handleForm(){
    if (this.editForm.invalid) {
       Object.values(this.editForm.controls).forEach((control) => {
         control.markAllAsTouched();
         control.markAsDirty();
       });
      return;
    } else {
      this.wss.editWorkshop(this.editForm,this.workshop.id).subscribe(data=>
        console.log("workshop ediatdo: ",data))
      this.edit=false;
    }
  }
}
