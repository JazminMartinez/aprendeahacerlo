"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.CollegeCareer = exports.UserRole = void 0;
var typeorm_1 = require("typeorm");
var WorkshopUser_entity_1 = require("./WorkshopUser.entity");
var UserRole;
(function (UserRole) {
    UserRole["INSTRUCTOR"] = "instructor";
    UserRole["STUDENT"] = "student";
})(UserRole = exports.UserRole || (exports.UserRole = {}));
var CollegeCareer;
(function (CollegeCareer) {
    CollegeCareer["ICO"] = "Ingenieria en Computacion";
    CollegeCareer["ISES"] = "Ingenieria en Sistemas Energeticos Sustentables";
    CollegeCareer["IEL"] = "Ingenieria Electronica";
    CollegeCareer["IME"] = "Ingenieria Mecanica";
    CollegeCareer["ICI"] = "Ingenieria Civil";
})(CollegeCareer = exports.CollegeCareer || (exports.CollegeCareer = {}));
var User = /** @class */ (function () {
    function User() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", String)
    ], User.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({
            type: "enum",
            enum: UserRole,
            default: UserRole.STUDENT
        }),
        __metadata("design:type", String)
    ], User.prototype, "role", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], User.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, default: "Lastname" }),
        __metadata("design:type", String)
    ], User.prototype, "lastName", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], User.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, default: 1 }),
        __metadata("design:type", Number)
    ], User.prototype, "semester", void 0);
    __decorate([
        typeorm_1.Column({ default: "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png" }),
        __metadata("design:type", String)
    ], User.prototype, "photo", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: true,
            type: "enum",
            enum: CollegeCareer,
            default: CollegeCareer.ICO
        }),
        __metadata("design:type", String)
    ], User.prototype, "collegeCareer", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, default: 1234567 }),
        __metadata("design:type", Number)
    ], User.prototype, "accountNumber", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "googleId", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return WorkshopUser_entity_1.WorkShopUser; }, function (workshopuser) { return workshopuser.user; }, { cascade: true }),
        typeorm_1.JoinColumn({ name: 'workshopuser_id' }),
        __metadata("design:type", Array)
    ], User.prototype, "workshopuser", void 0);
    User = __decorate([
        typeorm_1.Entity()
    ], User);
    return User;
}());
exports.User = User;
