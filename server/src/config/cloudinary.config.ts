import { v2 as cloudinary } from 'cloudinary';
const { CloudinaryStorage } = require('multer-storage-cloudinary');
const multer = require('multer');

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});

const storage = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: "imagenes", // foldername in cloudinary
    allowed_formats: ['mp4','ogv','jpg','png','pdf','gif']
  }
});

const uploadCloud = multer({ storage: storage });

export default uploadCloud;
