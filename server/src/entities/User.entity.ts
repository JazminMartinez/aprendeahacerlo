import { Entity, Column, PrimaryGeneratedColumn,OneToMany,JoinColumn } from 'typeorm';
import {WorkShopUser} from './WorkshopUser.entity'
export enum UserRole {
  INSTRUCTOR = 'instructor',
  STUDENT = 'student'
}

export enum CollegeCareer {
  ICO = 'Ingenieria en Computacion',
  ISES = 'Ingenieria en Sistemas Energeticos Sustentables',
  IEL='Ingenieria Electronica',
  IME="Ingenieria Mecanica",
  ICI="Ingenieria Civil"
}

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({
    type: "enum",
    enum: UserRole,
    default: UserRole.STUDENT
  })
  role: UserRole;

  @Column()
  name: string;

  @Column({ nullable: true,default:"Lastname"})
  lastName: string;

  @Column()
  email: string;

  @Column({ nullable: true})
  password: string;

  @Column({ nullable: true,default:1})
  semester: number;

  @Column({ default: "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"})
  photo: string;

  @Column({
    nullable: true,
    type: "enum",
    enum: CollegeCareer,
    default: CollegeCareer.ICO
  })
  collegeCareer: CollegeCareer;

  @Column({ nullable: true,default:1234567})
  accountNumber: number;

  @Column({ nullable: true})
  googleId: string;

  @OneToMany(type => WorkShopUser, workshopuser => workshopuser.user,{cascade: true})
    @JoinColumn({name: 'workshopuser_id'}) 
    workshopuser: WorkShopUser[]
}