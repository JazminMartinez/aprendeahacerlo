import { Entity, PrimaryGeneratedColumn,ManyToOne,JoinColumn } from "typeorm";
import { User } from './User.entity';
import { Workshop } from './Workshop.entity';

@Entity()
export class WorkShopUser{
    @PrimaryGeneratedColumn()
    id:number;

    @ManyToOne(type => User, user => user.workshopuser,{ onDelete: 'CASCADE' })
    @JoinColumn({ name: 'user_id' }) 
    user: User;

    @ManyToOne(type => Workshop, workshop => workshop.workshopuser,{ onDelete: 'CASCADE' })
    @JoinColumn({ name: 'workshop_id' }) 
    workshop: Workshop;
}