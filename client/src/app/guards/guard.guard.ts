import { Injectable } from '@angular/core';
import { Route, Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { take, skipWhile, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    // aca la version oauth de google
    if(route.parent.queryParams.authenticated) { 
      console.log('Query Params desde el Guard ', route.parent.queryParams);
      return true;
    }
    // esto verrifica si nos logueamos local
    return this.auth.isAutheticated.pipe(
      skipWhile((authenticated) => {
        return authenticated === null;
      }),
      take(1),
      tap((authenticated) => {
        if (!authenticated) {
          this.router.navigateByUrl('/auth/login');
        }
      })
    )
  }
  
}
