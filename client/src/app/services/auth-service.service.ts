
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject} from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from '../interfaces/user.interface';
import { Workshop } from '../interfaces/workshop.interface';

interface VerifyCredential {
  authenticated: boolean;
}
interface Available {
  available: boolean;
}

interface SignupForm {
  email:string,
  name:string,
  lastName:string,
  password: string,
  accountNumber:number,
  semester:number,
  photo?:string
}

interface SignupResponse {
  user: {
    name: string,
    lastName: string,
    email: string,
    password: string,
    semester: number,
    accountNumber: number,
    collegeCareer: string,
    googleId: string,
    id: number,
    role: string
  }
}

interface LoginForm {
  email: string;
  password: string;
}

interface LoginResponse {
  user: {
    id: number,
    role: string,
    name: string,
    lastName: string,
    email: string,
    password: string,
    semester: number,
    collegeCareer: string,
    accountNumber: number,
    googleId: string
  },
  authenticated: boolean
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //status=localStorage.getItem('status');
  isAutheticated = new BehaviorSubject(null);

  baseUrl = 'http://localhost:3000';

  //baseUrl = 'https://aprendeahacerlo.herokuapp.com';
  
  constructor(private http: HttpClient) { }

  validateEmail(value: string) {
    return this.http.post<Available>(this.baseUrl + '/api/emailAvailable', { email: value });
  }

  validateAccount(value: number) {
    return this.http.post<Available>(this.baseUrl + '/api/accountAvailable', { accountNumber: value });
  }

  verifyAuth() {
    return this.http.get<VerifyCredential>(this.baseUrl + '/api/loggedin').pipe(
      tap(({ authenticated }) => this.isAutheticated.next(authenticated))
    );
  }

  signup(value: any) {
    return this.http.post<SignupResponse>(this.baseUrl + '/api/auth/signup', value)
    .pipe(
      tap(val => {this.isAutheticated.next(true)
        })
    );
  }

  logout() {
    return this.http.get(this.baseUrl + '/api/auth/logout')
    .pipe(
      tap(val => this.isAutheticated.next(false))
    );
  }


  login(loginForm: LoginForm) {
    return this.http.post<LoginResponse>(this.baseUrl + '/api/auth/login', loginForm)
    .pipe(
      tap(val => this.isAutheticated.next(true))
    );
  }
  getProfile() {
    return this.http.get<User>(this.baseUrl + '/api/auth/profile');
  }

  google() {
    window.open(`${this.baseUrl}/api/auth/google`, '_self');
  }
}