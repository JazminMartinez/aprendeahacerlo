import { Request, Response, NextFunction } from "express";
import { User } from "../entities/User.entity";
import { getRepository } from "typeorm";
import bcrypt from "bcrypt";
import passport from "../config/passport.config";

/**
 * SIGNUP recibe los datos de req.body, enviados en el formulario.
 * Busca en la base de datos si no existe un usuario con ese email, si no encuentra uno, procede a crear uno nuevo,
 * Encripta la contraseña utilizando bcrypt y guarda todos los demas datos en la BD, si el proceso fue exitoso responde
 * con un status 200 y una respuesta JSON con un el usuario creado y un mensaje de exito.
 * En caso de que falle la operacion, responde con un status 500 y envia un mensaje y el error en formato JSON
 */
export const postSignup = (req: Request, res: Response, next: NextFunction) => {
  const {photo,name,lastName,email,password,accountNumber,semester,collegeCareer} = req.body;
  console.log("Que nos responde cloudinary: ", req.file);
  console.log(photo);
  console.log("este es el req.file del sugnup:",req.file);
  
  getRepository(User).findOne({ email }).then((user) => {
      if (!user) {
        const salt = bcrypt.genSaltSync(10);
        const hashPassword = bcrypt.hashSync(password, salt);
        let newUser:any;
        if (typeof req.file === "undefined") {
            newUser = getRepository(User).create({
            email,
            name,
            password: hashPassword,
            accountNumber,
            semester,
            collegeCareer,
            lastName
          });
        }else{
          const { path } = req.file;
            newUser = getRepository(User).create({
            email,
            name,
            password: hashPassword,
            accountNumber,
            semester,
            collegeCareer,
            lastName,
            photo:req.file.path
          });
        }
        getRepository(User)
          .save(newUser)
          .then((user) => {
            req.login(user, (err) => {
              if (err) {
                return next(err);
              } else console.log("success");
            });
            res.status(200).json({user});
          })
          .catch((err) => {
            console.log(err);
            res.status(500).json({ message: "Error", error: err });
          });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

/**
 * LOGGEDIN se encarga de verificar que se haya creado la propiedad req.user y si existe, se hace una consulta a la
 * BD para obtener el usuario y devuelve una respuesta con status 200 y en formato JSON envia una el usuario y una variable
 * llamada authenticated:true.
 * En caso de que no encuentre al usuario devuelve {authenticated:false}
 */
export const loggedin = async (req: any, res: Response, next: NextFunction) => {
  if (req.user) {
    const user = await getRepository(User).findOne(req.user.id);
    res.status(200).json({ authenticated: true});
  } else {
    res.status(200).json({ authenticated: false });
  }
};

/**
 * LOGGIN, una vez que ha pasado por el middleware de passport y haber creado la propiedad req.user, devuelve una repsuesta
 * con status 200 y en formato JSON el usuario, asi como la authenticated:true.
 * En caso de que exista un error, tal como credenciales invalidad (correo o password) devuelve una respuesta con status 500
 * y en formato JSON devuelve authenticated:false, el error y un mensaje de error.
 */
export const postLogin = (req: Request, res: Response, next: NextFunction) => {
  try {
    res.status(200).send({ user: req.user,authenticated: true });
  } catch (error) {
    res.status(500).json({message: "Invalid credentials",authenticated: false,error: error,});
  }
};

/**
 * VERIFY ACCOUNT busca en la BD si existe un usuario con el numero de cuenta que recibe del formulario 
 * en req.body.accountNumber. Si encuentra un usuario, el numero de cuenta no se encuentra disponible y 
 * devuelve una respuesta con status 200 y en formato JSON, envia available:false.
 * En caso de que no encuentre un usuario, significa que nadie lo ha registrado y se encuentra disponible,
 * y devuelve una respuesta con status 200 y en formato JSON, envia available:true.
 *  */
export const verifyAccount = async (req: Request, res: Response, next: NextFunction
) => {
  try {
    const user = await getRepository(User).findOne({ accountNumber: req.body.accountNumber });
    if (!user) {
      res.status(200).json({ available: true });
    } else {
      res.status(409).json({ available: false });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal server error", error: error });
  }
};

/**
 * VERIFY EMAIL busca en la BD si existe un usuario con el email que recibe del formulario 
 * en req.body.email. Si encuentra un usuario, el numero de cuenta no se encuentra disponible y 
 * devuelve una respuesta con status 200 y en formato JSON, envia available:false.
 * En caso de que no encuentre un usuario, significa que nadie lo ha registrado y se encuentra disponible,
 * y devuelve una respuesta con status 200 y en formato JSON, envia available:true.
 *  */
export const verifyEmail = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const user = await getRepository(User).findOne( { email: req.body.email});
    console.log(user);
    if(!user) {
      res.status(200).json({ available: true });
    } else {// nota para mi esto es un 200 clavado pero lo nevio con 409 por razones educativas
      res.status(409).json({ available: false });
    }
  } catch(error) {
    res.status(500).json({ message: 'Internal server error', error: error })
  }
}

/***
 * VERIFY ROL se encarga de checar si existe la propiedad req.user, en caso de que exista,
 * realiza una consulta a la
 */
export const verifyRol = async (req: any,res: Response,next: NextFunction) => {
  if (req.user) {
    const user = await getRepository(User).findOne(req.user.id);
    if (req.user.role === "instructor") {
      res.status(200).json({ instructor: true, user });
    } else if (req.user.role === "student") {
      res.status(200).json({ instructor: true, user });
    }
  } else {
    res.status(200).json({ authenticated: false });
  }
};

/**
 * PROFILE GET realiza una consulta a la base de datos y devuevle el usuario
 */
export const profileGet = async (req: any, res: Response, next: NextFunction) =>{
  if (req.user) {
    const user = await getRepository(User).findOne(req.user.id);
    res.status(200).json({ authenticated: true, user });
  } else {
    res.status(200).json({ authenticated: false });
  }
};

/**
 * DELETE USER se encarga de limpiar la cookie, destruir la session y eliminar al usurio
 *  de la BD, envia una respuesta con un mensaje y authenticated:false;
 */
export const deleteUser = async (req: Request,res: Response,next: Function) => {
  const { id } = req.params;
  try {
    req.logOut();
    res.status(200).clearCookie("connect.sid", {});
    req.session.destroy(() => {
      res.json({ mesagge: "Usuario eliminado con exito", authenticated: false });
    });
    await getRepository(User).delete(id);
  } catch (error) {
    res.status(500).json({ message: "Internal server error", error: error });
  }
};

/**
 * EDIT USER, recibe los datos del formulario con req.body y con el id que viene en req.params
 * actualiza el usuario con ese id y con los nuevos datos
 * 
 */
export const editUser = async (req: Request, res: Response, next: Function) => {
  const { id } = req.params;
  const {name,lastName,email,password,accountNumber,semester,collegeCareer} = req.body;
  const salt = bcrypt.genSaltSync(10);
  const hashPassword = bcrypt.hashSync(password, salt);
  try {
    await getRepository(User).update(id, {
      name,
      lastName,
      email,
      password: hashPassword,
      accountNumber,
      semester,
      collegeCareer,
    });
    const user = await getRepository(User).findOne(id);
    res.status(200).json({ message: "Usuario actualizado con exito", user });
  } catch (err) {
    res.status(500).json({message:"ocurrio un error" ,error: err });
  }
};

/**
 * LOGOUT devuelve una respuesta con status 200, se encarga de limpiar la cookie y destruir la session,
 * Envia authenticated:false en formato JSON
 */
export const logout = (req: any, res: Response, next: NextFunction) => {
  req.logOut();
  res.status(200).clearCookie("connect.sid", {});
  req.session.destroy(() => {
    res.json({ authenticated: false });
  });
};