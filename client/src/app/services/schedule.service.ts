import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  baseUrl = 'http://localhost:3000';
  //baseUrl = 'https://aprendeahacerlo.herokuapp.com';

  constructor(private http: HttpClient ) { }

  //servicio para traer los horarios de determinado taller
  getScheduleByWorkshop(id:number) {
    return this.http.get(this.baseUrl + `/api/${id}/getSchedules/` )
  }
}
