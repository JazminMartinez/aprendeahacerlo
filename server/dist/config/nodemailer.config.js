"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.confirmDelete = exports.confirmEdit = exports.confirmAccount = void 0;
var nodemailer_1 = __importDefault(require("nodemailer"));
var transporter = nodemailer_1.default.createTransport({
    host: 'smtp.gmail.com',
    auth: {
        user: "jazmin.94mh@gmail.com",
        pass: "sarangha3."
    }
});
exports.confirmAccount = function (para, workshop, user) { return __awaiter(void 0, void 0, void 0, function () {
    var day1, month1, fecha1, day2, month2, fecha2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                day1 = ("0" + workshop.startDate.getDate()).slice(-2);
                month1 = ("0" + (workshop.startDate.getMonth() + 1)).slice(-2);
                fecha1 = workshop.startDate.getFullYear() + "-" + (month1) + "-" + (day1);
                day2 = ("0" + workshop.endDate.getDate()).slice(-2);
                month2 = ("0" + (workshop.endDate.getMonth() + 1)).slice(-2);
                fecha2 = workshop.endDate.getFullYear() + "-" + (month2) + "-" + (day2);
                return [4 /*yield*/, transporter.sendMail({
                        from: '"Aprendiendo. Registro de talleres en linea. 😎" <jazmin.94mh@gmail.com>',
                        to: para,
                        subject: "Correo de confirmación de inscripción a taller",
                        html: "\n        <p style=\"text-align: center;\"><img src=\"https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG\" alt=\"Logotipo\" width=\"226\" height=\"108\" /></p>\n            <h2 style=\"text-align: center;\"><strong>Bienvenid@ " + user["name"] + " a " + workshop.name + "</strong></h2>\n            <div style=\"text-align: center;\"><strong>&nbsp;<h2>Fecha de inicio:</h2> <input type=\"date\" name=\"fecha\"  value=\"" + fecha1 + "\"></strong></div>\n            <div style=\"text-align: center;\"><strong><h2>Fecha de termino:</h2> <input type=\"date\" name=\"fecha\"  value=\"" + fecha2 + "\"></strong></div>\n            <div style=\"text-align: center;\">&nbsp;</div>\n            <h3 style=\"text-align: center;\"><strong>Podr&aacute;s ver m&aacute;s detalles de este taller, ingresando a tu perfil.</strong></h3>",
                    })];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
exports.confirmEdit = function (usuario, workshop) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, transporter.sendMail({
                    from: '"Aprendiendo. Registro de talleres en linea. 😎" <jazmin.94mh@gmail.com>',
                    to: usuario.email,
                    subject: "Taller actualizado",
                    html: "\n        <p style=\"text-align: center;\"><img src=\"https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG\" alt=\"Logotipo\" width=\"226\" height=\"108\" /></p>\n            <h2 style=\"text-align: center;\"><strong>" + workshop.name + "</strong></h2>\n            <h3 style=\"text-align: center;\"><strong>" + usuario["name"] + " por este medio, se te informa que el taller ha sido modificado.\n            Lamentamos el inconveniente y eperamos verlo en los nuevos horarios</strong></h3>\n            <h3 style=\"text-align: center;\"><strong>Podr&aacute;s ver m&aacute;s detalles de este taller, ingresando a tu perfil.</strong></h3>\n            <div style=\"text-align: center;\">&nbsp;</div>\n            ",
                })];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
exports.confirmDelete = function (usuario, workshop) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, transporter.sendMail({
                    from: '"Aprendiendo. Registro de talleres en linea. 😎" <jazmin.94mh@gmail.com>',
                    to: usuario.email,
                    subject: "Taller eliminado",
                    html: "\n        <p style=\"text-align: center;\"><img src=\"https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG\" alt=\"Logotipo\" width=\"226\" height=\"108\" /></p>\n            <h2 style=\"text-align: center;\"><strong>" + workshop.name + "</strong></h2>\n            <h3 style=\"text-align: center;\"><strong>" + usuario["name"] + " por este medio, se te informa que el taller ha sido eliminado \n            y a partir de hoy quedan canceladas las clases.\n            Lamentamos las molestias y esperamos que considere inscribirse a otro taller.</strong></h3>\n            <div style=\"text-align: center;\">&nbsp;</div>\n          ",
                })];
            case 1: return [2 /*return*/, _a.sent()];
        }
    });
}); };
