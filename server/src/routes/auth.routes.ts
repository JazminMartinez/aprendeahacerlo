import { Router } from 'express';
import passport from '../config/passport.config';
import uploadCloud from '../config/cloudinary.config';

import { 
  postSignup, 
  profileGet,
  postLogin, 
  logout,
  loggedin,
  verifyAccount,
  verifyRol,
  verifyEmail,
  editUser,
  deleteUser
} from '../controllers/auth.controller';

const router = Router();

router.post('/auth/signup',uploadCloud.single('photo'), postSignup);
router.post('/auth/login',passport.authenticate("local"), postLogin);
router.get('/auth/logout', logout);

router.get('/auth/profile', profileGet);
router.put('/auth/editUser/:id',editUser);
router.delete('/auth/deleteUser/:id',deleteUser);

router.get('/loggedin', loggedin);
router.get('/verifyRol', verifyRol);
router.post('/accountAvailable', verifyAccount);
router.post('/emailAvailable', verifyEmail);

// Google auth Routes
router.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email"
    ]
  })
);
router.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    failureRedirect: `${process.env.CLIENT_URL}/login`
  }), (req, res) => {
    console.log('El es usuario esta en sesion porque req.user = ', req.user)
    res.redirect(`${process.env.CLIENT_URL}/profile?authenticated=true`);
  }
);

export default router;