import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user.interface';
import { AuthService } from '../services/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {

  constructor(private auth:AuthService) { }

  resolve(router:ActivatedRouteSnapshot, state:RouterStateSnapshot):User | Observable<User>{
    return this.auth.getProfile();
   }
}
