import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { WorkshopsByCategoryService } from '../resolvers/workshops-by-category.service';
import { WorkshopsBCComponent } from './workshops-bc/workshops-bc.component';
import { WorkshopComponent } from './workshop/workshop.component';
import { WorkshopDetailsResolverService } from '../resolvers/workshop-details-resolver.service';
import { ScheduleResolverService } from '../resolvers/schedule-resolver.service';


const routes: Routes = [
  { path: 'categories', component: CategoriesComponent },
  { path: 'categories/:name', component: WorkshopsBCComponent,resolve: {
    workshops: WorkshopsByCategoryService
  } },
  { path: 'categories/:name/:id', component: WorkshopComponent, resolve: {
    workshop: WorkshopDetailsResolverService,
    schedule:ScheduleResolverService
  }
 },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopRoutingModule { }
