#!/usr/bin/env node
require("dotenv").config();

import "reflect-metadata";
import express from "express";
import logger from "morgan";
import path from "path";
import { createConnection } from "typeorm";

import cors from "cors";
import ExpressSession from "express-session";
import passport from "./config/passport.config";
import { TypeormStore } from "connect-typeorm/out";
import { Session } from "./entities/Session.entity";

import authRoutes from "./routes/auth.routes";
import workshopRoutes from "./routes/workshop.routes";
import scheduleRoutes from "./routes/schedule.routes";

async function bootstrap() {
  const app_name = require("../package.json").name;
  const debug = require("debug")(
    `${app_name}:${path.basename(__filename).split(".")[0]}`
  );

  const database_url =
    process.env.ENV === "development"
      ? `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@localhost:5432/${process.env.DB}`
      : process.env.DATABASE_URL;

  //DB connection
  const connection = await createConnection({
    type: "postgres",
    url: database_url,
    entities: [path.join(__dirname, "./entities/**/*.{ts,js}")],
    migrations: [path.join(__dirname, "./migrations/**/*.{ts,js}")],
    cli: {
      migrationsDir: "migrations",
    },
    synchronize: true,
  });
  // display DB info
  console.log(
    "connected to PostgreSQL! Database options: ",
    connection.options
  );

  // setup an express app
  const app = express();

  // Middleware Setup
  app.use(logger("dev"));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  // Configure express-session
  app.use(
    ExpressSession({
      secret: process.env.SECRET,
      resave: true,
      saveUninitialized: true,
      cookie: { maxAge: 1000 * 60 * 60 * 24 }, //sameSite: 'strict'
      store: new TypeormStore({
        cleanupLimit: 2,
        limitSubquery: false,
        ttl: 86400,
      }).connect(connection.getRepository(Session)),
    })
  );

  // passport
  app.use(passport.initialize());
  app.use(passport.session());

  // default value for title local
  app.locals.title = "Express - Generated with kavak typescript generator";

  // config of middleware cors

  app.use(
    cors({
      credentials: true,
      origin: [process.env.CLIENT_URL],
    })
  );

  // app.use(cors())
  // app.options('*', cors())
  // app.use(function(req, res, next) {
  //     res.header('Access-Control-Allow-Origin', '*')
  //     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  //     next()
  // })

  app.use("/api", authRoutes);
  app.use("/api", workshopRoutes);
  app.use("/api", scheduleRoutes);

  return app;
}

module.exports = bootstrap;
