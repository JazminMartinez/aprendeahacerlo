import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.addCookie(req)).pipe(
      tap(val => {
        if (val.type === HttpEventType.Sent) {
          console.log('Request sent: ');
        }
        if (val.type === HttpEventType.Response) {
          console.log('Response sent: ', val);
        }
      })
    )
  }

  private addCookie(request: HttpRequest<any>) {
    // if (!request.url.match(/http\:\/\/localhost:3000/) &&
    //     !request.url.match(/http\:\/\/aprendeahacerlo.herokuapp.com/)) {
    //   return request;
    // }
    return request.clone({
      withCredentials: true
    });
  }
}
