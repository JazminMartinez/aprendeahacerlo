"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteWorkshop = exports.editWorkshop = exports.mostRecentWorkshops = exports.unsubscribe = exports.subscribe = exports.getWorkshop = exports.getWorkshopsUser = exports.workshopsByCategories = exports.getAllWorkshops = exports.validateWorkshop = exports.createWorkshop = void 0;
var Workshop_entity_1 = require("../entities/Workshop.entity");
var typeorm_1 = require("typeorm");
var WorkshopUser_entity_1 = require("../entities/WorkshopUser.entity");
var nodemailer_config_1 = require("../config/nodemailer.config");
var User_entity_1 = require("../entities/User.entity");
/**
 *CREATE WORKSHOP se encarga de recibir los datos del formulario en req.body. Crea el nuevo workshop
  y lo salva en la base de datos, verifica que la operacion se haya hecho correctamente y responde
  con el taller creado. Ademas de crear la relacion entre un usuario y un taller.
  En caso de que exista un error, devuelve el error.
 */
exports.createWorkshop = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, category, name, capacity, description, startDate, endDate, name2, newWorkshop;
    return __generator(this, function (_b) {
        _a = req.body, category = _a.category, name = _a.name, capacity = _a.capacity, description = _a.description, startDate = _a.startDate, endDate = _a.endDate;
        name2 = name.toLowerCase().trim();
        try {
            newWorkshop = typeorm_1.getRepository(Workshop_entity_1.Workshop).create({
                category: category,
                name: name2,
                capacity: capacity,
                placesAvailable: capacity,
                description: description,
                startDate: startDate,
                endDate: endDate,
            });
            typeorm_1.getRepository(Workshop_entity_1.Workshop)
                .save(newWorkshop)
                .then(function (workshop) {
                res
                    .status(200)
                    .json({ workshop: workshop });
                var newWU = typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser).create({
                    user: req.user["id"],
                    workshop: workshop,
                });
                typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser).save(newWU);
            })
                .catch(function (err) {
                console.log(err);
                res.status(500).json({ error: err });
            });
        }
        catch (err) {
            res.status(500).json({ message: "ocurrio un error", error: err });
        }
        return [2 /*return*/];
    });
}); };
/**
 * VALIDATEWORKSHOP es un metodo que recibe el nombre que se desea colocar a un taller,
 * se recorre la BD en busca de un taller que incluya ese nombre
 * si existe, entonces el nombre no esta disponible y regresa false, en caso
 * contario devuelve true.
 */
exports.validateWorkshop = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var name, nameAvailable;
    return __generator(this, function (_a) {
        name = req.body.name;
        typeorm_1.getRepository(Workshop_entity_1.Workshop).find()
            .then(function (workshops) {
            var available = function (workshop) { return workshop.name.includes(name); };
            nameAvailable = !workshops.some(available);
            res.status(200).json({ nameAvailable: nameAvailable });
        })
            .catch(function (error) { return res.status(500).json({ message: "Internal server error", error: error }); });
        return [2 /*return*/];
    });
}); };
/**
 * GETALLWORKSHOPS  hace una consulta a la BD y devuelve todos los talleres.
 */
exports.getAllWorkshops = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var workshops, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop)
                        .createQueryBuilder("workshop")
                        .orderBy("workshop.createdAt", "ASC")
                        .where("workshop.placesAvailable >0")
                        .execute()];
            case 1:
                workshops = _a.sent();
                res.status(200).json(workshops);
                return [3 /*break*/, 3];
            case 2:
                err_1 = _a.sent();
                res.status(500).json({ message: "Ocurrio un error", error: err_1 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo se encarga de devolver los talleres cuya categoria corresponda con el
 * nombre que se le envia. En caso de que ocurra un error, se devulve el error.
 */
exports.workshopsByCategories = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var name, workshops, err_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                name = req.params.name;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop)
                        .createQueryBuilder("workshop")
                        .orderBy("workshop.name", "ASC")
                        .where("workshop.category = :category", { category: name })
                        .execute()];
            case 2:
                workshops = _a.sent();
                res.status(200).json(workshops);
                return [3 /*break*/, 4];
            case 3:
                err_2 = _a.sent();
                res.status(500).json({ message: "Error", error: err_2 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo hace una consulta a la BD de cada usuario, ya sea que los haya creado o se haya
 * unido al taller, utilizando el parametro req.user.id el cual se crea cuando el usuario
 * inicia sesion.
 */
exports.getWorkshopsUser = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, userId, workshops, err_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                user = req.user;
                userId = user["id"];
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop)
                        .createQueryBuilder("workshop")
                        .innerJoinAndSelect("workshop.workshopuser", "workshopuser")
                        .where("workshopuser.user_id = :user_id", { user_id: userId })
                        .execute()];
            case 2:
                workshops = _a.sent();
                res.status(200).json(workshops);
                return [3 /*break*/, 4];
            case 3:
                err_3 = _a.sent();
                res.status(500).json({ message: "Error", error: err_3 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo se utiliza para devolver el taller en especifico. Enviando el id del taller,
 * se realiza la consulta en la base de datos y si encuentra el taller, lo devuelve y si no
 * devuelve un mensaje de no encontrado. En caso de algun error del servidor, lo devuelve.
 */
exports.getWorkshop = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, workshop, err_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 2:
                workshop = _a.sent();
                if (workshop) {
                    res.status(200).json(workshop);
                }
                else {
                    res.status(404).json({ mesage: "Taller no encontrado" });
                }
                return [3 /*break*/, 4];
            case 3:
                err_4 = _a.sent();
                res.status(500).json({ error: err_4 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
/**
 * Este método se encarga de recibir el id del taller al que se quiere suscribir un usuario y
 * creara una relacion entre un usuario y taller y ademas enviara un correo a su email.
 * En caso de que no encuentre el taller, devuelve un mensaje de taller no encontrado.
 */
exports.subscribe = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, user, userId, email, workshop, workshopuser, newWSU, pa, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                user = req.user;
                userId = user["id"];
                email = user["email"];
                _a.label = 1;
            case 1:
                _a.trys.push([1, 10, , 11]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 2:
                workshop = _a.sent();
                if (!workshop) return [3 /*break*/, 8];
                return [4 /*yield*/, typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser)
                        .createQueryBuilder("workshopuser")
                        .where("workshopuser.user_id = :user_id", { user_id: userId })
                        .andWhere("(workshopuser.workshop_id = :workshop_id)", {
                        workshop_id: id,
                    })
                        .getOne()];
            case 3:
                workshopuser = _a.sent();
                if (!workshopuser) return [3 /*break*/, 4];
                res.status(200).json({ message: "el usuario ya esta registrado" });
                return [3 /*break*/, 7];
            case 4:
                newWSU = typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser).create({
                    user: userId,
                    workshop: workshop,
                });
                pa = workshop.placesAvailable - 1;
                typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser).save(newWSU);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).update(id, { placesAvailable: pa })];
            case 5:
                _a.sent();
                return [4 /*yield*/, nodemailer_config_1.confirmAccount(email, workshop, req.user)];
            case 6:
                _a.sent();
                res.status(200).json({ message: "El usuario se registro a un taller", newWSU: newWSU });
                _a.label = 7;
            case 7: return [3 /*break*/, 9];
            case 8:
                res.status(404).json({ mesage: "Taller no encontrado" });
                _a.label = 9;
            case 9: return [3 /*break*/, 11];
            case 10:
                error_1 = _a.sent();
                res.status(500).json({ message: "Error", error: error_1 });
                return [3 /*break*/, 11];
            case 11: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo se encarga de buscar en la BD el taller al cual el usuario desea desuscribirse,
 * en caso de que lo encuentre elimina la relacion entre taller y usuario y actualiza el
 * numero de lugares disponibles del taller. En caso de que no lo encuentre solo envia un mensaje
 * de no encontrado.
 */
exports.unsubscribe = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, user, userId, workshopuser, idWorkshopiuser, workshopActual, pa, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                user = req.user;
                userId = user["id"];
                _a.label = 1;
            case 1:
                _a.trys.push([1, 8, , 9]);
                return [4 /*yield*/, typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser)
                        .createQueryBuilder("workshopuser")
                        .where("workshopuser.user_id = :user_id", { user_id: userId })
                        .andWhere("(workshopuser.workshop_id = :workshop_id)", {
                        workshop_id: id,
                    })
                        .getOne()];
            case 2:
                workshopuser = _a.sent();
                if (!workshopuser) return [3 /*break*/, 6];
                idWorkshopiuser = workshopuser.id;
                return [4 /*yield*/, typeorm_1.getRepository(WorkshopUser_entity_1.WorkShopUser).delete(idWorkshopiuser)];
            case 3:
                _a.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 4:
                workshopActual = _a.sent();
                pa = workshopActual.placesAvailable + 1;
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).update(id, { placesAvailable: pa })];
            case 5:
                _a.sent();
                res.status(200).json({ message: "Se elimino al usuario del taller" });
                return [3 /*break*/, 7];
            case 6:
                res.status(404).json({ mesage: "Taller no encontrado" });
                _a.label = 7;
            case 7: return [3 /*break*/, 9];
            case 8:
                error_2 = _a.sent();
                res.status(500).json({ message: "Error", error: error_2 });
                return [3 /*break*/, 9];
            case 9: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo realiza una consulta a la tabla de workshop y trae los primeros 10 talleres
 * mas recientes, ordenados por fecha en orden descendente.
 * En caso de que exista un error, solo lo devuelve como respuesta
 */
exports.mostRecentWorkshops = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var workshops, err_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop)
                        .createQueryBuilder("workshop")
                        .orderBy("workshop.createdAt", "DESC")
                        .limit(10)
                        .execute()];
            case 1:
                workshops = _a.sent();
                res.status(200).json(workshops);
                return [3 /*break*/, 3];
            case 2:
                err_5 = _a.sent();
                res.status(500).json({ message: "Error", error: err_5 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo permite editar un taller, si lo encuentra en la BD y si no, solo envia un mensaje
 *  de taller no encontrado. En caso de un error del servdior, solo devuevle el error.
 */
exports.editWorkshop = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, category, name, capacity, description, startDate, endDate, workshopActual, capacidadActual, lugaresDispActual, lugaresOcupados, nuevosLugaresDisponibles, workshop, workshops, i, idUser, usuario, error_3;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, category = _a.category, name = _a.name, capacity = _a.capacity, description = _a.description, startDate = _a.startDate, endDate = _a.endDate;
                _b.label = 1;
            case 1:
                _b.trys.push([1, 13, , 14]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 2:
                workshopActual = _b.sent();
                if (!workshopActual) return [3 /*break*/, 11];
                capacidadActual = workshopActual.capacity;
                lugaresDispActual = workshopActual.placesAvailable;
                lugaresOcupados = capacidadActual - lugaresDispActual;
                nuevosLugaresDisponibles = capacity - lugaresOcupados;
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).update(id, {
                        category: category,
                        name: name,
                        capacity: capacity,
                        placesAvailable: nuevosLugaresDisponibles,
                        description: description,
                        startDate: startDate,
                        endDate: endDate
                    })];
            case 3:
                _b.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 4:
                workshop = _b.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop)
                        .createQueryBuilder("workshop")
                        .innerJoinAndSelect("workshop.workshopuser", "workshopuser")
                        .where("workshopuser.workshop_id = :workshop_id", { workshop_id: id })
                        .execute()];
            case 5:
                workshops = _b.sent();
                i = 0;
                _b.label = 6;
            case 6:
                if (!(i < workshops.length)) return [3 /*break*/, 10];
                idUser = workshops[i].workshopuser_user_id;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(idUser)];
            case 7:
                usuario = _b.sent();
                return [4 /*yield*/, nodemailer_config_1.confirmEdit(usuario, workshop)];
            case 8:
                _b.sent();
                _b.label = 9;
            case 9:
                i++;
                return [3 /*break*/, 6];
            case 10:
                res.status(200).json({ message: "Workshop actualizado con exito", workshop: workshop });
                return [3 /*break*/, 12];
            case 11:
                res.status(404).json({ mesage: "No existe el taller" });
                _b.label = 12;
            case 12: return [3 /*break*/, 14];
            case 13:
                error_3 = _b.sent();
                res.status(500).json({ message: "ocurrio un error", error: error_3 });
                return [3 /*break*/, 14];
            case 14: return [2 /*return*/];
        }
    });
}); };
/**
 * Este metodo recibe un id en la propiedad req.params del id del taller a eliminar.
 * En caso de que lo encuentre, lo elimina, en caso de que no lo encuentre solo
 * devuelve un mensaje de no encontrado.
 * Si ocurre algun error por parte del servidor, devuelve el error
 */
exports.deleteWorkshop = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, workshopActual, workshop, workshops, i, idUser, usuario, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 13, , 14]);
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 2:
                workshopActual = _a.sent();
                if (!workshopActual) return [3 /*break*/, 11];
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).findOne(id)];
            case 3:
                workshop = _a.sent();
                return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop)
                        .createQueryBuilder("workshop")
                        .innerJoinAndSelect("workshop.workshopuser", "workshopuser")
                        .where("workshopuser.workshop_id = :workshop_id", { workshop_id: id })
                        .execute()];
            case 4:
                workshops = _a.sent();
                console.log(workshops);
                i = 0;
                _a.label = 5;
            case 5:
                if (!(i < workshops.length)) return [3 /*break*/, 9];
                idUser = workshops[i].workshopuser_user_id;
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(idUser)];
            case 6:
                usuario = _a.sent();
                console.log(usuario.email);
                return [4 /*yield*/, nodemailer_config_1.confirmDelete(usuario, workshop)];
            case 7:
                _a.sent();
                _a.label = 8;
            case 8:
                i++;
                return [3 /*break*/, 5];
            case 9: return [4 /*yield*/, typeorm_1.getRepository(Workshop_entity_1.Workshop).delete(id)];
            case 10:
                _a.sent();
                res.status(200).json({ message: "Taller eliminado con exito" });
                return [3 /*break*/, 12];
            case 11:
                res.status(404).json({ message: "Taller no encontrado" });
                _a.label = 12;
            case 12: return [3 /*break*/, 14];
            case 13:
                error_4 = _a.sent();
                res.status(500).json({ message: "Internal server error", error: error_4 });
                return [3 /*break*/, 14];
            case 14: return [2 /*return*/];
        }
    });
}); };
// export const workshopsUser = async (req: Request,res: Response,next: NextFunction) => {
// getRepository(WorkShopUser).find({ relations: ["workshop","user"] })
// .then(workshopsuser=>res.status(200).json({workshopsuser}))
// .catch(error=>res.status(500).json({ message: "Internal server error", error: error }))
// }
