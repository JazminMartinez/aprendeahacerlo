import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkshopRoutingModule } from './workshop-routing.module';
import { CategoriesComponent } from './categories/categories.component';
import { ComponentsModule } from '../components/components.module';
import { WorkshopsBCComponent } from './workshops-bc/workshops-bc.component';
import { WorkshopComponent } from './workshop/workshop.component';


@NgModule({
  declarations: [CategoriesComponent, WorkshopsBCComponent, WorkshopComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    WorkshopRoutingModule
  ]
})
export class WorkshopModule { }
