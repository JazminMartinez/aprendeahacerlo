import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ScheduleService } from '../services/schedule.service';

@Injectable({
  providedIn: 'root'
})
export class ScheduleResolverService implements Resolve<any>{

  constructor(private ss: ScheduleService) { }

  resolve(router:ActivatedRouteSnapshot, state:RouterStateSnapshot):any | Observable<any>{
    return this.ss.getScheduleByWorkshop(router.params.id);
   }
}
