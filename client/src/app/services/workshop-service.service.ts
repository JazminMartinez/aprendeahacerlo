import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Workshop } from '../interfaces/workshop.interface';

@Injectable({
  providedIn: 'root'
})
export class WorkshopServiceService {
  baseUrl = 'http://localhost:3000';

  /*baseUrl = 'https://aprendeahacerlo.herokuapp.com';*/
  
  constructor(private http: HttpClient ) { }

  //servicio que trae de vuelta los talleres mas recientemnete creados
  getMostRecentWorkshops() {
    return this.http.get(this.baseUrl + '/api/mostRecentWorkshops' )
  }

  //servicio que devuelve un taller en especifico
  getWorkshop(id:number) {
    return this.http.get(this.baseUrl + '/api/workshop/'+id )
  }

  //servicio que trae de vuelta aquellos talleres cuya categoria sea la indicada
  getWorkshopsByCategories(name:string) {
    return this.http.get(this.baseUrl + '/api/workshops/'+name);
  }

  //servicio para registrar un usuario en el taller
  getSubscribe(id:number){
    return this.http.get(this.baseUrl+'/api/subscribe/'+id)
  }
  
  //servicio que devuelve los talleres del usuario.
  getWorkshopsUser(){
    return this.http.get(this.baseUrl+'/api/workshopsUser')
  }

  //servicio que elimina un taller,basado en el id que se le envia
  deleteWorkshop(id:any){
    return this.http.delete(this.baseUrl+'/api/deleteWorkshop/'+id)
  }

  //este servicio elimina la relación de un alumno inscrito a un taller
  unsubscribeWorkshop(id:number){
    return this.http.delete(this.baseUrl+'/api/unsubscribe/'+id)
  }

  //servicio para editar un taller
  editWorkshop(form:any,id:any){
    return this.http.post(this.baseUrl + '/api/editWorkshop/'+id, form)
  }
}