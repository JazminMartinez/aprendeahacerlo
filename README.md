![alt text](https://1.bp.blogspot.com/-7hWpJY6gVCE/Xx5zAPmNnKI/AAAAAAAAACg/onCeEHVYwHwsaCtz1D_avh2jYoYa5VvkACLcBGAsYHQ/s385/logo.PNG "Logo Aprende a hacerlo")
# Aprende a hacerlo.
> Este proyecto consiste en un registro en linea a talleres que se imparten de manera presencial en las escuelas. Existen dos tipos de roles.
1. **Instructor**, que puede realizar las siguientes acciones:
    * Crear un cuenta con rol de instructor.
    * Ingresar y ver su página de perfil.
    * Editar su perfil
    * Crear un nuevo taller.
        * Ingresar los datos del taller.
        * Agregar los horarios del taller
    * Ver los talleres que imparte
    * Editar y/o eliminar un taller que imparte

2. **Alumno**, el cual puede realizar:
    * Crear un cuenta con rol de Alumno.
    * Ingresar y ver su página de perfil.
    * Editar su perfil
    * Ver y registrarse en los talleres que se encuentran disponibles
    * Ver los talleres a los que se ha registrado

Puntos que hicieron falta cubrir:
    * Editar usuario.
    * Editar taller.
    * Actualizar los datos una vez que se modifica, elimina o muestra.
    * Crear talleres y horarios.



