import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { UserResolverService } from '../resolvers/user-resolver.service';
import { WorkshopsUserResolverService } from '../resolvers/workshops-user-resolver.service';
import { WorkshopsUserComponent } from '../components/Shared/workshops-user/workshops-user.component';
import { WorkshopDetailComponent } from '../components/Shared/workshop-detail/workshop-detail.component';
import { WorkshopDetailsResolverService } from '../resolvers/workshop-details-resolver.service';
import { ScheduleResolverService } from '../resolvers/schedule-resolver.service';


const routes: Routes = [
  {path:'',component:ProfileComponent,resolve:{ user : UserResolverService }},
  {path:'workshopsUser',component:WorkshopsUserComponent,resolve:{ 
    workshops : WorkshopsUserResolverService,
    user : UserResolverService 
  }
},
{path:'workshopsUser/:id',component:WorkshopDetailComponent, resolve: {
  workshop: WorkshopDetailsResolverService,
  schedule:ScheduleResolverService,
  user : UserResolverService 
}}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
