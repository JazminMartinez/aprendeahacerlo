"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var passport_config_1 = __importDefault(require("../config/passport.config"));
var cloudinary_config_1 = __importDefault(require("../config/cloudinary.config"));
var auth_controller_1 = require("../controllers/auth.controller");
var router = express_1.Router();
router.post('/auth/signup', cloudinary_config_1.default.single('photo'), auth_controller_1.postSignup);
router.post('/auth/login', passport_config_1.default.authenticate("local"), auth_controller_1.postLogin);
router.get('/auth/logout', auth_controller_1.logout);
router.get('/auth/profile', auth_controller_1.profileGet);
router.put('/auth/editUser/:id', auth_controller_1.editUser);
router.delete('/auth/deleteUser/:id', auth_controller_1.deleteUser);
router.get('/loggedin', auth_controller_1.loggedin);
router.get('/verifyRol', auth_controller_1.verifyRol);
router.post('/accountAvailable', auth_controller_1.verifyAccount);
router.post('/emailAvailable', auth_controller_1.verifyEmail);
// Google auth Routes
router.get("/auth/google", passport_config_1.default.authenticate("google", {
    scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/userinfo.email"
    ]
}));
router.get("/auth/google/callback", passport_config_1.default.authenticate("google", {
    failureRedirect: process.env.CLIENT_URL + "/login"
}), function (req, res) {
    console.log('El es usuario esta en sesion porque req.user = ', req.user);
    res.redirect(process.env.CLIENT_URL + "/profile?authenticated=true");
});
exports.default = router;
