import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { MostRecentWorkshopsResolverService } from './resolvers/most-recent-workshops-resolver.service';
import { UserResolverService } from './resolvers/user-resolver.service';
import { GuardGuard } from './guards/guard.guard';


const routes: Routes = [
  { path: '', component: HomeComponent,
     resolve: {
       workshops: MostRecentWorkshopsResolverService,
       //user:UserResolverService
     }  
  },

  { path :'auth', loadChildren:()=>import('./auth/auth.module').then(m=>m.AuthModule)},

  { path : 'profile', canActivate: [ GuardGuard ],
  loadChildren:()=>import('./protected/protected.module').then(m=>m.ProtectedModule)},

  { path : 'workshop', loadChildren:()=>import('./workshop/workshop.module').then(m=>m.WorkshopModule)} ,
  
  { path: '**', pathMatch: 'full', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
