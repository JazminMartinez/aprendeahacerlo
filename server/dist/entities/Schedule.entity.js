"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Schedule = void 0;
var typeorm_1 = require("typeorm");
var Workshop_entity_1 = require("./Workshop.entity");
var Schedule = /** @class */ (function () {
    function Schedule() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Schedule.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Schedule.prototype, "day", void 0);
    __decorate([
        typeorm_1.CreateDateColumn({ type: "time" }),
        __metadata("design:type", typeorm_1.Timestamp)
    ], Schedule.prototype, "startTime", void 0);
    __decorate([
        typeorm_1.CreateDateColumn({ type: "time" }),
        __metadata("design:type", typeorm_1.Timestamp)
    ], Schedule.prototype, "endTime", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Workshop_entity_1.Workshop; }, function (workshop) { return workshop.schedule; }, { onDelete: 'CASCADE' }),
        typeorm_1.JoinColumn({ name: 'workshop_id' }),
        __metadata("design:type", Workshop_entity_1.Workshop)
    ], Schedule.prototype, "workshop", void 0);
    Schedule = __decorate([
        typeorm_1.Entity()
    ], Schedule);
    return Schedule;
}());
exports.Schedule = Schedule;
