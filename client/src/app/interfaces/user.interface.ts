export interface User {
    id:string
    role: string;
    name: string;
    lastName: string;
    email: string;
    password: string;
    semester: number;
    photo: string;
    collegeCareer: string;
    accountNumber: number;
    googleId: string;
}
