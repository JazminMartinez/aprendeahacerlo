export interface Workshop{
    id: number;
    category: string;
    name: string;
    capacity: number;
    placesAvailable: number;
    description: string;
    startDate: Date;
    endDate: Date;
    createdAt: Date;
}