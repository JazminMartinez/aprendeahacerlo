import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service.service';
import { UniqueEmailService } from 'src/app/services/unique-email.service';
import { UniqueAccountService } from 'src/app/services/unique-account.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: [
  ]
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  foto='';
  show:boolean=false;
  carreras=[
    {name : 'Ingenieria en Computacion'},
    {name : 'Ingenieria en Sistemas Energeticos Sustentables'},
    {name : 'Ingenieria Electronica'},
    {name : "Ingenieria Mecanica"},
    {name : "Ingenieria Civil"}
  ]
  constructor(private fb: FormBuilder, private auth: AuthService, 
    private router: Router ,private ues:UniqueEmailService,
    private uncs:UniqueAccountService) { }

  ngOnInit(): void {
    this.signupForm = this.fb.group({
      name: ["", [Validators.required, Validators.pattern(/^[a-zA-Z]*$/)]],
      lastName: ["", [Validators.required,Validators.pattern(/^[a-zA-Z]*$/)]],
      email: ["",[
          Validators.required,
          Validators.pattern(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)],this.ues.validate],
      password: ["", [Validators.required,Validators.minLength(8),Validators.maxLength(8)]],
      accountNumber: ["",[Validators.required, Validators.minLength(7),Validators.maxLength(7),],this.uncs.validate],
      semester: ["",[Validators.required, Validators.min(1),Validators.max(10)]],
      collegeCareer: ["", [Validators.required]],
      photo: ['']
    });
  }

  showPreview(event) {
    if (event.target.files.length > 0) {
      this.show=true;
      const file = event.target.files[0];
      this.signupForm.value.photo=file;
       const reader = new FileReader();
       reader.onload = () => {
         this.foto = reader.result as string;
       };
       reader.readAsDataURL(file);
    }
  }

  handleForm() {
    if (this.signupForm.invalid) {
      Object.values(this.signupForm.controls).forEach(control => {
        control.markAsTouched();
        control.markAsDirty();
      });
      
      return;
    } else {
      const formData = new FormData();
      for ( const key of Object.keys(this.signupForm.value) ) {
        formData.append(key, this.signupForm.value[key]);
      }

       this.auth.signup(formData).subscribe( {
         next: (res) => {
           this.router.navigateByUrl('/profile');
         },
         error: (err) => {
           if(err.status === 0) {
             this.signupForm.setErrors({ unknownError: true });
           }
         }
       });
    }
  }
  get isInvalid() {
    return this.signupForm.controls.collegeCareer.touched 
            && this.signupForm.controls.collegeCareer.dirty
            && this.signupForm.controls.collegeCareer.invalid
            && this.signupForm.controls.collegeCareer.errors.required;
  }
  reset(){
    this.foto='';
    this.show=false;
    this.signupForm.reset({name: '', lastName: '', email: '', password: '',semester: '',
    photo: '',collegeCareer: '', accountNumber: ''});

  }
}
