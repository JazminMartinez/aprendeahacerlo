import { Component, OnInit,Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: [
  ]
})
export class InputComponent implements OnInit {
  @Input() inputFormControl: FormControl;
  @Input() label: string;
  @Input() type: string;
  @Input() placeholder: string;
  constructor() { }

  get isInvalid() {
    return this.inputFormControl.touched && this.inputFormControl.dirty && this.inputFormControl.errors;
  }
  get isRequiredInvalid() {
    return this.inputFormControl.errors.required;
  }
  get isMinLengthInvalid() {
    return this.inputFormControl.errors.minlength;
  }
  get isMaxLengthInvalid() {
    return this.inputFormControl.errors.maxlength;
  }
   get isPatternInvalid() {
    return this.inputFormControl.errors.pattern;
   }
  get isPatternInvalidEmail() {
    return this.inputFormControl.errors.pattern.requiredPattern ===  "/[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$/";
  }
  get isMinInvalid() {
    return this.inputFormControl.errors.min;
  }
  get isMaxInvalid() {
    return this.inputFormControl.errors.max;
  }
  //error que se crea cuando el email ya esta en uso
  get isUniqueEmail() {
    return this.inputFormControl.errors.notAvailable;
  }

  get isUniqueAccount() {
    return this.inputFormControl.errors.notAvailableAccount;
  }

  ngOnInit(): void {
  }

}
