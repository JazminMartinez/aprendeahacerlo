import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service.service';
import { WorkshopServiceService } from 'src/app/services/workshop-service.service';

@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.component.html',
  styles: [
  ]
})
export class WorkshopComponent implements OnInit {
  workshop:any;
  schedule:any;
  horarios:boolean=false;
  isAuthenticated: boolean=false;
  mensaje:string='';
  show:boolean=false;
  loading:boolean=false;
  constructor(private router: ActivatedRoute,private auth:AuthService,private whs:WorkshopServiceService,
    private routerA:Router) { }

  ngOnInit(): void {
    //verificamos si el usuario ha iniciado sesion para poder ver los componentes
    this.auth.isAutheticated.subscribe(status=>{
      this.isAuthenticated=status;
     });
     
     //asigna la informacion traida del resolver
     this.router.data.subscribe(data => {
      this.workshop = data.workshop;
      this.schedule = data.schedule.schedules;
      if(this.schedule.length===0){
        this.horarios=true;
      }else{
        this.horarios=false;
      }
    })
  }

  /**
   * cuando un usuario da click en registrar se llama al servicio para inscibirlo en el taller
   * si ya esta registrado no lo registra y le envia un mensaje
   */
  inscribirse(){
    this.loading=true;
    this.whs.getSubscribe(this.workshop.id).subscribe({
      next: (resp:any) => {
        this.show=true;
        if(resp.message==="el usuario ya esta registrado"){
          this.mensaje="El usuario ya esta registrado en este taller";
          
        }else{
          this.mensaje="Se ha registrado al usuario con éxito, se ha enviado un correo";

          this.router.data.subscribe(data => {
            this.workshop = data.workshop;
          })
          console.log("envia los datos de nuevo", this.workshop);

        }
        this.loading=false;
      },
      error: ({ error }) => {
        console.log(error);
      }
    })
  }

  cerrarModal(value:boolean){
    this.show=value;
  }
}