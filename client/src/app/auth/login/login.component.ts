import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ["",[
          Validators.required,
          Validators.pattern(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]],
      password: ["", [Validators.required,Validators.minLength(8),Validators.maxLength(8)]]
    });
  }

  handleForm() {
    if (this.loginForm.invalid) {
      Object.values(this.loginForm.controls).forEach(control => {
        control.markAsTouched();
        control.markAsDirty();
      });
      
      return;
    } else{
      this.auth.login(this.loginForm.value).subscribe({
        next: () => {
          console.log("Entre!! en el componente de login ");
          this.router.navigateByUrl('/profile');
        },
        error: ({ error }) => {
          if (error === 'Unauthorized') {
            this.loginForm.setErrors({ invalidCredentials: true });
          } else {
            this.loginForm.setErrors( { unknownError: true } );
          }
        }
      });
    }
  }
  reset(){
    this.loginForm.reset({ email: '', password: ''});
  }

  loginWithGoogle() {
    this.auth.google();
  }
}
