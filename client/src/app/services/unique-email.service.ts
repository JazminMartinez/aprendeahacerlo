import { Injectable } from '@angular/core';
import { AsyncValidator, FormControl } from '@angular/forms';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { AuthService } from 'src/app/services/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class UniqueEmailService implements AsyncValidator {

  constructor(private auth: AuthService) { }

  validate = (control: FormControl) => {
    const { value } = control;
    return this.auth.validateEmail(value).pipe(
        map(resp => null),
        catchError(err => {
          if (!err.error.available) {
            return of({ notAvailable: true});
          } else {
            return of({ noConnection: true});
          }
        })
      );
  }
}
