import { Request, Response, NextFunction } from "express";
import { Workshop } from "../entities/Workshop.entity";
import { getRepository, Any } from "typeorm";
import { WorkShopUser } from "../entities/WorkshopUser.entity";
import { confirmAccount,confirmEdit,confirmDelete } from '../config/nodemailer.config';
import { User } from "../entities/User.entity";

/**
 *CREATE WORKSHOP se encarga de recibir los datos del formulario en req.body. Crea el nuevo workshop 
  y lo salva en la base de datos, verifica que la operacion se haya hecho correctamente y responde 
  con el taller creado. Ademas de crear la relacion entre un usuario y un taller. 
  En caso de que exista un error, devuelve el error.
 */
export const createWorkshop = async (req: Request,res: Response,next: NextFunction) => {
  const {category,name,capacity,description,startDate,endDate} = req.body;
  let name2=name.toLowerCase().trim();
  try {
    const newWorkshop = getRepository(Workshop).create({
      category,
      name:name2,
      capacity,
      placesAvailable: capacity,
      description,
      startDate,
      endDate,
    });

    getRepository(Workshop)
      .save(newWorkshop)
      .then((workshop) => {res
      .status(200)
      .json(  {workshop} );
      const newWU = getRepository(WorkShopUser).create({
        user: req.user["id"],
        workshop: workshop,
      });
      getRepository(WorkShopUser).save(newWU);
      })
      .catch((err) => {console.log(err);
      res.status(500).json({error: err} );
      });
  } catch (err) {
    res.status(500).json({ message: "ocurrio un error", error: err });
  }
};

/**
 * VALIDATEWORKSHOP es un metodo que recibe el nombre que se desea colocar a un taller,
 * se recorre la BD en busca de un taller que incluya ese nombre
 * si existe, entonces el nombre no esta disponible y regresa false, en caso
 * contario devuelve true.
 */
export const validateWorkshop=async (req: Request, res: Response, next: NextFunction) => {
  const {name}=req.body;
  let nameAvailable:boolean;
  getRepository(Workshop).find()
  .then(workshops=>{
    const available = (workshop:Workshop) => workshop.name.includes(name);
    nameAvailable=!workshops.some(available);
    res.status(200).json({nameAvailable:nameAvailable});

  })
  .catch(error=>res.status(500).json({ message: "Internal server error", error: error }))
};

/**
 * GETALLWORKSHOPS  hace una consulta a la BD y devuelve todos los talleres.
 */
export const getAllWorkshops = async (req: Request,res: Response,next: NextFunction) => {
  try {
    const workshops = await getRepository(Workshop)
      .createQueryBuilder("workshop")
      .orderBy("workshop.createdAt", "ASC")
      .where("workshop.placesAvailable >0")
      .execute();
      res.status(200).json( workshops );
  } catch (err) {
    res.status(500).json({ message: "Ocurrio un error", error: err });
  }
};

/**
 * Este metodo se encarga de devolver los talleres cuya categoria corresponda con el
 * nombre que se le envia. En caso de que ocurra un error, se devulve el error.
 */
export const workshopsByCategories = async (req: Request,res: Response,next: NextFunction) => {
  const { name } = req.params;
  try {
    const workshops = await getRepository(Workshop)
      .createQueryBuilder("workshop")
      .orderBy("workshop.name", "ASC")
      .where("workshop.category = :category", { category: name })
      .execute();
    res.status(200).json( workshops );
  } catch (err) {
    res.status(500).json({ message: "Error", error: err });
  }
};

/**
 * Este metodo hace una consulta a la BD de cada usuario, ya sea que los haya creado o se haya
 * unido al taller, utilizando el parametro req.user.id el cual se crea cuando el usuario 
 * inicia sesion.
 */
export const getWorkshopsUser = async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req;
  const userId = user["id"];
  try {
    const workshops = await getRepository(Workshop)
      .createQueryBuilder("workshop")
      .innerJoinAndSelect("workshop.workshopuser", "workshopuser")
      .where("workshopuser.user_id = :user_id", { user_id: userId })
      .execute();
    res.status(200).json( workshops );
  } catch (err) {
    res.status(500).json({ message: "Error", error: err });
  }
};

/**
 * Este metodo se utiliza para devolver el taller en especifico. Enviando el id del taller, 
 * se realiza la consulta en la base de datos y si encuentra el taller, lo devuelve y si no
 * devuelve un mensaje de no encontrado. En caso de algun error del servidor, lo devuelve.
 */
export const getWorkshop = async (req: Request,res: Response,next: NextFunction) => {
  const { id } = req.params;
  try {
    const workshop = await getRepository(Workshop).findOne(id);
    if (workshop) {
      res.status(200).json( workshop );
    } else {
      res.status(404).json({ mesage: "Taller no encontrado" });
    }
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

/**
 * Este método se encarga de recibir el id del taller al que se quiere suscribir un usuario y
 * creara una relacion entre un usuario y taller y ademas enviara un correo a su email.
 * En caso de que no encuentre el taller, devuelve un mensaje de taller no encontrado.
 */
export const subscribe = async (req: Request,res: Response,next: NextFunction) => {
  const { id } = req.params;
  const { user } = req;
  const userId = user["id"];
  const email=user["email"];

  try {
    const workshop = await getRepository(Workshop).findOne(id);
    if (workshop) {
      //si encuentra el taller, va a buscar si el usuario ya esta registrado en el taller
      const workshopuser = await getRepository(WorkShopUser)
      .createQueryBuilder("workshopuser")
      .where("workshopuser.user_id = :user_id", { user_id: userId })
      .andWhere("(workshopuser.workshop_id = :workshop_id)", {
        workshop_id: id,
      })
      .getOne();
      //si encuentra que ya esta registrado, entonces solo envia un mensaje
      if(workshopuser){
        res.status(200).json({ message: "el usuario ya esta registrado"});
      }else{
        //pero si el usuario no esta registrado, crea una nueva relacion y la guarda
        const newWSU = getRepository(WorkShopUser).create({
          user: userId,
          workshop: workshop,
        });
        let pa = workshop.placesAvailable - 1;
  
        getRepository(WorkShopUser).save(newWSU);
        await getRepository(Workshop).update(id, { placesAvailable: pa });
        await confirmAccount(email,workshop,req.user);
        res.status(200).json({ message: "El usuario se registro a un taller", newWSU });
      }
    } else {
      res.status(404).json({ mesage: "Taller no encontrado" });
    }
  } catch (error) {
    res.status(500).json({ message: "Error", error: error });
  }
};

/**
 * Este metodo se encarga de buscar en la BD el taller al cual el usuario desea desuscribirse,
 * en caso de que lo encuentre elimina la relacion entre taller y usuario y actualiza el
 * numero de lugares disponibles del taller. En caso de que no lo encuentre solo envia un mensaje
 * de no encontrado.
 */
export const unsubscribe = async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params;
  const { user } = req;
  const userId = user["id"];

  try {
    const workshopuser = await getRepository(WorkShopUser)
      .createQueryBuilder("workshopuser")
      .where("workshopuser.user_id = :user_id", { user_id: userId })
      .andWhere("(workshopuser.workshop_id = :workshop_id)", {
        workshop_id: id,
      })
      .getOne();

    if (workshopuser) {
      let idWorkshopiuser = workshopuser.id;
      await getRepository(WorkShopUser).delete(idWorkshopiuser);
      const workshopActual = await getRepository(Workshop).findOne(id);
      let pa = workshopActual.placesAvailable + 1;
      await getRepository(Workshop).update(id, { placesAvailable: pa });
      res.status(200).json({ message: "Se elimino al usuario del taller" });
    } else {
      res.status(404).json({ mesage: "Taller no encontrado" });
    }
  } catch (error) {
    res.status(500).json({ message: "Error", error: error });
  }
};

/**
 * Este metodo realiza una consulta a la tabla de workshop y trae los primeros 10 talleres
 * mas recientes, ordenados por fecha en orden descendente.
 * En caso de que exista un error, solo lo devuelve como respuesta
 */
export const mostRecentWorkshops = async (req: Request,res: Response,next: NextFunction) => {
  try {
    const workshops = await getRepository(Workshop)
      .createQueryBuilder("workshop")
      .orderBy("workshop.createdAt", "DESC")
      .limit(10)
      .execute();
    res.status(200).json(workshops);
  } catch (err) {
    res.status(500).json({ message: "Error", error: err });
  }
};

/**
 * Este metodo permite editar un taller, si lo encuentra en la BD y si no, solo envia un mensaje
 *  de taller no encontrado. En caso de un error del servdior, solo devuevle el error.
 */
export const editWorkshop = async (req: Request,res: Response,next: NextFunction) => {
  const { id } = req.params;
  const {category, name, capacity, description, startDate, endDate} = req.body;
  try {
    const workshopActual = await getRepository(Workshop).findOne(id);
    if (workshopActual) {
      const capacidadActual = workshopActual.capacity;
      const lugaresDispActual = workshopActual.placesAvailable;
      const lugaresOcupados = capacidadActual - lugaresDispActual;
      const nuevosLugaresDisponibles = capacity - lugaresOcupados;
  
      await getRepository(Workshop).update(id, {
        category,
        name,
        capacity,
        placesAvailable: nuevosLugaresDisponibles,
        description,
        startDate,
        endDate
        });

      const workshop = await getRepository(Workshop).findOne(id);
      const workshops = await getRepository(Workshop)
      .createQueryBuilder("workshop")
      .innerJoinAndSelect("workshop.workshopuser", "workshopuser")
      .where("workshopuser.workshop_id = :workshop_id", { workshop_id: id })
      .execute();
      for(let i =0; i<workshops.length;i++){
        let idUser=workshops[i].workshopuser_user_id;
        let usuario= await getRepository(User).findOne(idUser);
        await confirmEdit(usuario,workshop);
      }

      res.status(200).json({ message: "Workshop actualizado con exito", workshop });
    } else {
      res.status(404).json({ mesage: "No existe el taller" });
    }
  } catch (error) {
    res.status(500).json({ message: "ocurrio un error", error: error });
  }
};

/**
 * Este metodo recibe un id en la propiedad req.params del id del taller a eliminar.
 * En caso de que lo encuentre, lo elimina, en caso de que no lo encuentre solo
 * devuelve un mensaje de no encontrado.
 * Si ocurre algun error por parte del servidor, devuelve el error 
 */
export const deleteWorkshop = async (req: Request,res: Response,next: NextFunction) => {
  const { id } = req.params;
  try {
    const workshopActual = await getRepository(Workshop).findOne(id);
    if(workshopActual){
     
      const workshop = await getRepository(Workshop).findOne(id);
      const workshops = await getRepository(Workshop)
      .createQueryBuilder("workshop")
      .innerJoinAndSelect("workshop.workshopuser", "workshopuser")
      .where("workshopuser.workshop_id = :workshop_id", { workshop_id: id })
      .execute();
      console.log(workshops)
      for(let i =0; i<workshops.length;i++){
        let idUser=workshops[i].workshopuser_user_id;
        let usuario= await getRepository(User).findOne(idUser);
        console.log( usuario.email);
        await confirmDelete(usuario,workshop);
      }
      await getRepository(Workshop).delete(id);
      res.status(200).json({ message: "Taller eliminado con exito" });
    }else{
      res.status(404).json({ message: "Taller no encontrado" });
    }
  } catch (error) {
    res.status(500).json({ message: "Internal server error", error: error });
  }
};

// export const workshopsUser = async (req: Request,res: Response,next: NextFunction) => {
// getRepository(WorkShopUser).find({ relations: ["workshop","user"] })
// .then(workshopsuser=>res.status(200).json({workshopsuser}))
// .catch(error=>res.status(500).json({ message: "Internal server error", error: error }))
// }
