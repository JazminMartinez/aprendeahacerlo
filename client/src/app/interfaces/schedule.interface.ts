import { Time } from '@angular/common';

export interface Schedule{
    id:number;
    day:string;
    startTime:Time;
    endTime:Time;
}