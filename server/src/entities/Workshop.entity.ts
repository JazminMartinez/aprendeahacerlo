import { Entity, Column, PrimaryGeneratedColumn,OneToMany,JoinColumn,CreateDateColumn } from 'typeorm';
import {WorkShopUser} from './WorkshopUser.entity'
import { Schedule } from './Schedule.entity';

export enum Categories {
  DANCE = 'Baile',
  MUSIC = 'Musica',
  LANGUAGES = 'Idiomas',
  SPORTS = 'Deportes',
  ART  = 'Arte',
  OTHER='Otro'
}

@Entity()
export class Workshop {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: true,
    type: "enum",
    enum: Categories,
    default: Categories.OTHER
  })
  category: Categories;

  @Column()
  name: string;

  @Column()
  capacity: number;

  @Column()
  placesAvailable: number;

  @Column()
  description: string;

  @CreateDateColumn({ nullable: true})
  startDate: Date;

  @CreateDateColumn({ nullable: true})
  endDate: Date;

  @CreateDateColumn()
  createdAt: Date;


  @OneToMany(type => WorkShopUser, workshopuser => workshopuser.workshop)
    @JoinColumn({name: 'workshopuser_id'}) // es opcional en relaciones MtO (pero ayuda a renombrar fields)
    workshopuser: WorkShopUser[]


  @OneToMany(type => Schedule, schedule => schedule.workshop)
    @JoinColumn({name: 'schedule_id'}) 
    schedule: Schedule[]
}