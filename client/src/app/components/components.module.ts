import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './Shared/navbar/navbar.component';
import { WorkshopCardComponent } from './Shared/workshop-card/workshop-card.component';
import { RouterModule } from '@angular/router';
import { InputComponent } from './Shared/input/input.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './Shared/footer/footer.component';
import { CategoriesCardComponent } from './Shared/categories-card/categories-card.component';
import { ModalComponent } from './Shared/modal/modal.component';
import { NotificationComponent } from './Shared/notification/notification.component';
import { SubscribeComponent } from './Shared/subscribe/subscribe.component';
import { ThereIsNotComponent } from './Shared/there-is-not/there-is-not.component';
import { UserInstructorComponent } from './Shared/user-instructor/user-instructor.component';
import { WorkshopsUserComponent } from './Shared/workshops-user/workshops-user.component';
import { WorkshopDetailComponent } from './Shared/workshop-detail/workshop-detail.component';
import { InputsEditComponent } from './inputs-edit/inputs-edit.component';

//declaramos y exportamos los componentes que son compartidos
@NgModule({
  declarations: [NavbarComponent, WorkshopCardComponent, 
    InputComponent, FooterComponent, CategoriesCardComponent, ModalComponent, 
    NotificationComponent, SubscribeComponent, ThereIsNotComponent, UserInstructorComponent, 
    WorkshopsUserComponent, WorkshopDetailComponent, InputsEditComponent],

  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ],
  
  exports:[NavbarComponent,WorkshopCardComponent,InputComponent,
    FooterComponent,CategoriesCardComponent,ModalComponent,NotificationComponent,SubscribeComponent,
    ThereIsNotComponent,UserInstructorComponent,WorkshopsUserComponent,WorkshopDetailComponent,InputsEditComponent]
})
export class ComponentsModule { }
