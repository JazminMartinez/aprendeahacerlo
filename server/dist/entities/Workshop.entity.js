"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Workshop = exports.Categories = void 0;
var typeorm_1 = require("typeorm");
var WorkshopUser_entity_1 = require("./WorkshopUser.entity");
var Schedule_entity_1 = require("./Schedule.entity");
var Categories;
(function (Categories) {
    Categories["DANCE"] = "Baile";
    Categories["MUSIC"] = "Musica";
    Categories["LANGUAGES"] = "Idiomas";
    Categories["SPORTS"] = "Deportes";
    Categories["ART"] = "Arte";
    Categories["OTHER"] = "Otro";
})(Categories = exports.Categories || (exports.Categories = {}));
var Workshop = /** @class */ (function () {
    function Workshop() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Workshop.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({
            nullable: true,
            type: "enum",
            enum: Categories,
            default: Categories.OTHER
        }),
        __metadata("design:type", String)
    ], Workshop.prototype, "category", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Workshop.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Workshop.prototype, "capacity", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Workshop.prototype, "placesAvailable", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Workshop.prototype, "description", void 0);
    __decorate([
        typeorm_1.CreateDateColumn({ nullable: true }),
        __metadata("design:type", Date)
    ], Workshop.prototype, "startDate", void 0);
    __decorate([
        typeorm_1.CreateDateColumn({ nullable: true }),
        __metadata("design:type", Date)
    ], Workshop.prototype, "endDate", void 0);
    __decorate([
        typeorm_1.CreateDateColumn(),
        __metadata("design:type", Date)
    ], Workshop.prototype, "createdAt", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return WorkshopUser_entity_1.WorkShopUser; }, function (workshopuser) { return workshopuser.workshop; }),
        typeorm_1.JoinColumn({ name: 'workshopuser_id' }) // es opcional en relaciones MtO (pero ayuda a renombrar fields)
        ,
        __metadata("design:type", Array)
    ], Workshop.prototype, "workshopuser", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Schedule_entity_1.Schedule; }, function (schedule) { return schedule.workshop; }),
        typeorm_1.JoinColumn({ name: 'schedule_id' }),
        __metadata("design:type", Array)
    ], Workshop.prototype, "schedule", void 0);
    Workshop = __decorate([
        typeorm_1.Entity()
    ], Workshop);
    return Workshop;
}());
exports.Workshop = Workshop;
