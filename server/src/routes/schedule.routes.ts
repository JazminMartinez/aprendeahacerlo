import { Router } from 'express';
import { createSchedule, getSchedules,editSchedule,deleteSchedule,validateDay} from '../controllers/schedule.controller';

const router = Router();
router.post('/:id/createSchedule/', createSchedule);
router.post('/:id/validateDay', validateDay);
router.get('/:id/getSchedules/', getSchedules);
router.put('/:id/editSchedule/:idS', editSchedule);
router.delete('/:id/deleteSchedule/:idS', deleteSchedule);


export default router;