import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/interfaces/categories.interface';

@Component({
  selector: 'app-categories-card',
  templateUrl: './categories-card.component.html',
  styles: [
  ]
})
export class CategoriesCardComponent implements OnInit {
  @Input() categories:Category;
  constructor() { }

  ngOnInit(): void {
  }

}
