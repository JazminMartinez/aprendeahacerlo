import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-workshop-card',
  templateUrl: './workshop-card.component.html',
  styles: [
  ]
})
export class WorkshopCardComponent implements OnInit {
  //Recibe los talleres que le envia el componente padre
  @Input() workshops:any;

  constructor(){}
  
  ngOnInit(): void {}
}
