"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logout = exports.editUser = exports.deleteUser = exports.profileGet = exports.verifyRol = exports.verifyEmail = exports.verifyAccount = exports.postLogin = exports.loggedin = exports.postSignup = void 0;
var User_entity_1 = require("../entities/User.entity");
var typeorm_1 = require("typeorm");
var bcrypt_1 = __importDefault(require("bcrypt"));
/**
 * SIGNUP recibe los datos de req.body, enviados en el formulario.
 * Busca en la base de datos si no existe un usuario con ese email, si no encuentra uno, procede a crear uno nuevo,
 * Encripta la contraseña utilizando bcrypt y guarda todos los demas datos en la BD, si el proceso fue exitoso responde
 * con un status 200 y una respuesta JSON con un el usuario creado y un mensaje de exito.
 * En caso de que falle la operacion, responde con un status 500 y envia un mensaje y el error en formato JSON
 */
exports.postSignup = function (req, res, next) {
    var _a = req.body, photo = _a.photo, name = _a.name, lastName = _a.lastName, email = _a.email, password = _a.password, accountNumber = _a.accountNumber, semester = _a.semester, collegeCareer = _a.collegeCareer;
    console.log("Que nos responde cloudinary: ", req.file);
    console.log(photo);
    console.log("este es el req.file del sugnup:", req.file);
    typeorm_1.getRepository(User_entity_1.User).findOne({ email: email }).then(function (user) {
        if (!user) {
            var salt = bcrypt_1.default.genSaltSync(10);
            var hashPassword = bcrypt_1.default.hashSync(password, salt);
            var newUser = void 0;
            if (typeof req.file === "undefined") {
                newUser = typeorm_1.getRepository(User_entity_1.User).create({
                    email: email,
                    name: name,
                    password: hashPassword,
                    accountNumber: accountNumber,
                    semester: semester,
                    collegeCareer: collegeCareer,
                    lastName: lastName
                });
            }
            else {
                var path = req.file.path;
                newUser = typeorm_1.getRepository(User_entity_1.User).create({
                    email: email,
                    name: name,
                    password: hashPassword,
                    accountNumber: accountNumber,
                    semester: semester,
                    collegeCareer: collegeCareer,
                    lastName: lastName,
                    photo: req.file.path
                });
            }
            typeorm_1.getRepository(User_entity_1.User)
                .save(newUser)
                .then(function (user) {
                req.login(user, function (err) {
                    if (err) {
                        return next(err);
                    }
                    else
                        console.log("success");
                });
                res.status(200).json({ user: user });
            })
                .catch(function (err) {
                console.log(err);
                res.status(500).json({ message: "Error", error: err });
            });
        }
    })
        .catch(function (err) {
        console.log(err);
        res.status(500).json({ error: err });
    });
};
/**
 * LOGGEDIN se encarga de verificar que se haya creado la propiedad req.user y si existe, se hace una consulta a la
 * BD para obtener el usuario y devuelve una respuesta con status 200 y en formato JSON envia una el usuario y una variable
 * llamada authenticated:true.
 * En caso de que no encuentre al usuario devuelve {authenticated:false}
 */
exports.loggedin = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(req.user.id)];
            case 1:
                user = _a.sent();
                res.status(200).json({ authenticated: true });
                return [3 /*break*/, 3];
            case 2:
                res.status(200).json({ authenticated: false });
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * LOGGIN, una vez que ha pasado por el middleware de passport y haber creado la propiedad req.user, devuelve una repsuesta
 * con status 200 y en formato JSON el usuario, asi como la authenticated:true.
 * En caso de que exista un error, tal como credenciales invalidad (correo o password) devuelve una respuesta con status 500
 * y en formato JSON devuelve authenticated:false, el error y un mensaje de error.
 */
exports.postLogin = function (req, res, next) {
    try {
        res.status(200).send({ user: req.user, authenticated: true });
    }
    catch (error) {
        res.status(500).json({ message: "Invalid credentials", authenticated: false, error: error, });
    }
};
/**
 * VERIFY ACCOUNT busca en la BD si existe un usuario con el numero de cuenta que recibe del formulario
 * en req.body.accountNumber. Si encuentra un usuario, el numero de cuenta no se encuentra disponible y
 * devuelve una respuesta con status 200 y en formato JSON, envia available:false.
 * En caso de que no encuentre un usuario, significa que nadie lo ha registrado y se encuentra disponible,
 * y devuelve una respuesta con status 200 y en formato JSON, envia available:true.
 *  */
exports.verifyAccount = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ accountNumber: req.body.accountNumber })];
            case 1:
                user = _a.sent();
                if (!user) {
                    res.status(200).json({ available: true });
                }
                else {
                    res.status(409).json({ available: false });
                }
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                res.status(500).json({ message: "Internal server error", error: error_1 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * VERIFY EMAIL busca en la BD si existe un usuario con el email que recibe del formulario
 * en req.body.email. Si encuentra un usuario, el numero de cuenta no se encuentra disponible y
 * devuelve una respuesta con status 200 y en formato JSON, envia available:false.
 * En caso de que no encuentre un usuario, significa que nadie lo ha registrado y se encuentra disponible,
 * y devuelve una respuesta con status 200 y en formato JSON, envia available:true.
 *  */
exports.verifyEmail = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne({ email: req.body.email })];
            case 1:
                user = _a.sent();
                console.log(user);
                if (!user) {
                    res.status(200).json({ available: true });
                }
                else { // nota para mi esto es un 200 clavado pero lo nevio con 409 por razones educativas
                    res.status(409).json({ available: false });
                }
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                res.status(500).json({ message: 'Internal server error', error: error_2 });
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
/***
 * VERIFY ROL se encarga de checar si existe la propiedad req.user, en caso de que exista,
 * realiza una consulta a la
 */
exports.verifyRol = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(req.user.id)];
            case 1:
                user = _a.sent();
                if (req.user.role === "instructor") {
                    res.status(200).json({ instructor: true, user: user });
                }
                else if (req.user.role === "student") {
                    res.status(200).json({ instructor: true, user: user });
                }
                return [3 /*break*/, 3];
            case 2:
                res.status(200).json({ authenticated: false });
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * PROFILE GET realiza una consulta a la base de datos y devuevle el usuario
 */
exports.profileGet = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var user;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (!req.user) return [3 /*break*/, 2];
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(req.user.id)];
            case 1:
                user = _a.sent();
                res.status(200).json({ authenticated: true, user: user });
                return [3 /*break*/, 3];
            case 2:
                res.status(200).json({ authenticated: false });
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };
/**
 * DELETE USER se encarga de limpiar la cookie, destruir la session y eliminar al usurio
 *  de la BD, envia una respuesta con un mensaje y authenticated:false;
 */
exports.deleteUser = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = req.params.id;
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                req.logOut();
                res.status(200).clearCookie("connect.sid", {});
                req.session.destroy(function () {
                    res.json({ mesagge: "Usuario eliminado con exito", authenticated: false });
                });
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).delete(id)];
            case 2:
                _a.sent();
                return [3 /*break*/, 4];
            case 3:
                error_3 = _a.sent();
                res.status(500).json({ message: "Internal server error", error: error_3 });
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); };
/**
 * EDIT USER, recibe los datos del formulario con req.body y con el id que viene en req.params
 * actualiza el usuario con ese id y con los nuevos datos
 *
 */
exports.editUser = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, name, lastName, email, password, accountNumber, semester, collegeCareer, salt, hashPassword, user, err_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, name = _a.name, lastName = _a.lastName, email = _a.email, password = _a.password, accountNumber = _a.accountNumber, semester = _a.semester, collegeCareer = _a.collegeCareer;
                salt = bcrypt_1.default.genSaltSync(10);
                hashPassword = bcrypt_1.default.hashSync(password, salt);
                _b.label = 1;
            case 1:
                _b.trys.push([1, 4, , 5]);
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).update(id, {
                        name: name,
                        lastName: lastName,
                        email: email,
                        password: hashPassword,
                        accountNumber: accountNumber,
                        semester: semester,
                        collegeCareer: collegeCareer,
                    })];
            case 2:
                _b.sent();
                return [4 /*yield*/, typeorm_1.getRepository(User_entity_1.User).findOne(id)];
            case 3:
                user = _b.sent();
                res.status(200).json({ message: "Usuario actualizado con exito", user: user });
                return [3 /*break*/, 5];
            case 4:
                err_1 = _b.sent();
                res.status(500).json({ message: "ocurrio un error", error: err_1 });
                return [3 /*break*/, 5];
            case 5: return [2 /*return*/];
        }
    });
}); };
/**
 * LOGOUT devuelve una respuesta con status 200, se encarga de limpiar la cookie y destruir la session,
 * Envia authenticated:false en formato JSON
 */
exports.logout = function (req, res, next) {
    req.logOut();
    res.status(200).clearCookie("connect.sid", {});
    req.session.destroy(function () {
        res.json({ authenticated: false });
    });
};
